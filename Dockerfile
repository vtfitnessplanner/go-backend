# Use the official Golang image as base
FROM golang:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the Go modules manifest and the source code
COPY go.mod .
COPY go.sum .

# Download dependencies
RUN go mod download

# Copy the rest of the source code
COPY . .

# Build the Go app
RUN go build -o server .

# Expose the port the server will run on
EXPOSE 8080

# Command to run the executable
CMD ["./server"]
