package util

import (
	"fmt"
	"log"
	"net/http"
)

// HandleWriteError handles errors that relate to writing into ResponseWriter.
// Returns true if the passed instance of an error is not nil.
// Just documents the output to the logger.
func HandleWriteError(err error) bool {
	if err != nil {
		log.Printf("Failed to write a reponse when calling w.Write due to the following error:\n%s\n", err)
		return true
	}
	return false
}

// HandleServerError handles all the errors that are related to issues occuring on the server side.
// Returns true if err is not nil.
// If err is not nil, documents the error and writes an appropriate response to the client.
func HandleServerError(err error, op string, w http.ResponseWriter) bool {
	if err != nil {
		log.Printf("Failed %s due to the following error:\n %s\n", op, err)
		errStr := fmt.Sprintf("Failed %s when processing request.", op)
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte(errStr))
		HandleWriteError(err)
		return true
	}
	return false
}

// HandleUserError handles all the errors that are related to issues occuring due to user error.
// Returns back the value of cond.
// If cond is not nil, documents the error and writes the errReason to the client.
func HandleUserError(cond bool, errReason string, w http.ResponseWriter) bool {
	if cond {
		log.Printf("Bad request recieved: %s \n", errReason)
		errStr := fmt.Sprintf("Bad request: %s \n", errReason)
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte(errStr))
		HandleWriteError(err)
		return true
	}
	return false
}

// HandleInvalidRoute writes back a resposne to the client that the route requested is invalid.
func HandleInvalidRoute(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNotFound)
	_, err := w.Write([]byte("Route does not exist"))
	HandleWriteError(err)
}
