package util

import (
	"database/sql"
	"net/http"
	"net/url"
	"strconv"
	"time"

	firebase "firebase.google.com/go/v4"
)

// ProtectRoute a wrapper function that will check that the user is logged in and pass the
// user's info into f.
func ProtectRoute(app *firebase.App,
	db *sql.DB,
	w http.ResponseWriter,
	r *http.Request,
	f func(w2 http.ResponseWriter, r2 *http.Request, userId int),
) {
	token := ValidateLogin(app, w, r)
	if token == nil {
		return
	}

	stmt := `
			SELECT User.userID
			FROM User
			WHERE User.userAuthToken = ?;`

	row := db.QueryRow(stmt, token.UID)
	var userId int
	err := row.Scan(&userId)

	if HandleServerError(err, "Provided token was valid, but was unable to find user in the database", w) {
		return
	}

	f(w, r, userId)
}

// RespondWithSuccess sends back reponse to the client with status code 200 and a message.
func RespondWithSuccess(w http.ResponseWriter, msg []byte) {
	w.WriteHeader(http.StatusOK)
	_, err := w.Write(msg)
	HandleWriteError(err)
}

// GetRouteDateParameter returns a date parsed from the given url_str. If an error occurs, writes the error to
// the response writer and return false in the second value. If a false is returned, the returned time is not
// meaningful.
func GetRouteDateParameter(url_str string, w http.ResponseWriter) (time.Time, bool) {
	query, err := url.ParseQuery(url_str)
	if HandleUserError(err != nil, "Invalid request parameters", w) {
		return time.Time{}, false
	}
	dateStr := query.Get("date")
	// This can be changed at some later time to provide a non paramerized route.
	if HandleUserError(dateStr == "", "Did not provide date for the food", w) {
		return time.Time{}, false
	}

	date, err := time.Parse("20060102", dateStr)
	if HandleServerError(err, "parsing date", w) {
		return time.Time{}, false
	}

	return date, true
}

// GetRouteIdParameter returns the foodId parsed from the given url_str. If an errors occurs, writes the error to
// the response writer and retuns false in the second value. If a false is returned, the returned id is not
// meaningful.
func GetRouteIdParameter(url_str string, w http.ResponseWriter) (uint, bool) {
	query, err := url.ParseQuery(url_str)
	if HandleServerError(err, "Parsing request URL", w) {
		return 0, false
	}
	mealIdStr := query.Get("foodId")
	if HandleUserError(mealIdStr == "", "missing Id parameter", w) {
		return 0, false
	}
	mealId, err := strconv.ParseUint(mealIdStr, 10, 32)
	if HandleUserError(err != nil, "Unable to parse mealId parameter", w) {
		return 0, false
	}
	return uint(mealId), true
}
