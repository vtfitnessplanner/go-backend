package util

import (
	"context"
	"net/http"

	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/auth"
)

// Purpose of this package is to provide methods that interact with firebase,
// without the need to constantly reuse the same functions all the time.

// GetAuthClient returns an *auth.Client based on the current firebase app and handles errors that
// relate to its initialization. Returns nil if initialization fails and handles the error that
// relates to it.
func GetAuthClient(app *firebase.App, w http.ResponseWriter, ctx context.Context) *auth.Client {
	client, err := app.Auth(ctx)
	if HandleServerError(err, "Initialize firebase Authentication", w) {
		return nil
	}
	return client
}

// GetUserRecord returns a user record from firebase. If an error occurs, documents it, writes a response
// to the user and returns nil.
func GetUserRecord(app *firebase.App, w http.ResponseWriter, r *http.Request) *auth.UserRecord {
	token := ValidateLogin(app, w, r)
	if token == nil {
		return nil
	}
	client := GetAuthClient(app, w, r.Context())
	if client == nil {
		return nil
	}
	userRecord, err := client.GetUser(r.Context(), token.UID)
	if HandleServerError(err, "to retrieve user data from firebase", w) {
		return nil
	}
	return userRecord
}

// ValidateLogin If everything goes smoothly, return a firebase auth token. Else write a response to the client and return nil.
func ValidateLogin(app *firebase.App, w http.ResponseWriter, r *http.Request) *auth.Token {
	client := GetAuthClient(app, w, r.Context())
	if client == nil {
		return nil
	}
	appCheckToken := r.Header.Get("X-Firebase-AppCheck")
	if HandleUserError(appCheckToken == "", "Header 'X-Firebase-AppCheck' is empty", w) {
		return nil
	}

	token, err := client.VerifyIDToken(r.Context(), appCheckToken)
	if HandleUserError(err != nil, "Invalid user token.", w) {
		return nil
	}

	return token
}
