// Package routes contains all the functions that relate to the routes.
// This specific file handles all the routes that go to ".../auth/..."
package routes

import (
	"database/sql"
	"encoding/json"
	"errors"
	"go_backend/util"
	"net/http"
	"regexp"
	"time"

	firebase "firebase.google.com/go/v4"
)

type authHandler struct {
	app *firebase.App
	db  *sql.DB
}

type registrationRequest struct {
	FirstName string `json:"fname"`
	LastName  string `json:"lname"`
	DOB       string `json:"DOB"`
	Gender    string `json:"gender"`
}

// NewAuthHandler create a new authHandler. Users should not interact with authHandler directly.
func NewAuthHandler(fb *firebase.App, db *sql.DB) (http.Handler, error) {
	if fb == nil {
		return nil, errors.New("provided firebase app pointer is nil")
	}
	if db == nil {
		return nil, errors.New("provided database pointer is nil")
	}

	return authHandler{fb, db}, nil
}

// ServeHTTP handles the requests that proceed to .../auth/ and redirects them to helper
// functions.
func (h authHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	registerRegex := regexp.MustCompile(`^/?auth/register/?`)
	switch {
	case r.Method == http.MethodPost && registerRegex.MatchString(r.URL.Path):
		h.registerUser(w, r)
	default:
		util.HandleInvalidRoute(w)
	}
}

// registerUser Handles user registration at ".../auth/register"
// The call must provide the following headers:
//
//	X-Firebase-AppCheck
//
// The call must also provide the following body:
//
//	fnmae (str): First name of the user.
//	lname (str): Last name of the user.
//	DOB (str): Date of birth of the user.
//	gender (str): User's gender.
//
// Upon successful registration will return status 200.
func (h authHandler) registerUser(w http.ResponseWriter, r *http.Request) {
	userRecord := util.GetUserRecord(h.app, w, r)
	// Verify that the user does not exist in the database already.
	var count int
	err := h.db.QueryRow("SELECT COUNT(*) FROM User WHERE User.userEmail = ?", userRecord.Email).Scan(&count)
	if util.HandleServerError(err, "Counting number of existing users", w) {
		return
	}

	if util.HandleUserError(count > 0, "User already exists", w) {
		return
	}

	// Insert the new user into the table.
	var req registrationRequest
	err = json.NewDecoder(r.Body).Decode(&req)
	if util.HandleUserError(err != nil, "Invalid request body", w) {
		return
	}

	stmt, err := h.db.Prepare("INSERT INTO User(userFname, userLname, userEmail, userDOB, userGender, userAuthToken) VALUES(?, ?, ?, ?, ?, ?)")
	if util.HandleServerError(err, "Creating a new user", w) {
		return
	}
	defer stmt.Close()
	dateOfBirth, err := time.Parse(time.DateOnly, req.DOB)
	if util.HandleUserError(err != nil, "Invalid date fromat", w) {
		return
	}
	_, err = stmt.Exec(req.FirstName, req.LastName, userRecord.Email, dateOfBirth, req.Gender, userRecord.UID)
	if util.HandleServerError(err, "Creating a new user", w) {
		return
	}
	util.RespondWithSuccess(w, []byte("User created successfully."))
}
