// Package routes contains all the functions that relate to the routes.
// This specific file handles all the routes that go to ".../auth/..."
package routes

import (
	"database/sql"
	"encoding/json"
	"errors"
	"github.com/guregu/null/v5/zero"
	"go_backend/util"
	"net/http"
	"regexp"

	firebase "firebase.google.com/go/v4"
)

type userHandler struct {
	db *sql.DB
	fb *firebase.App
}

// NewUserHandler creates userHandler. Users should not interact with userHandler directly.
func NewUserHandler(fb *firebase.App, db *sql.DB) (http.Handler, error) {
	if db == nil {
		return nil, errors.New("provided database is nil")
	}
	if fb == nil {
		return nil, errors.New("provided firebase app is nil")
	}
	return userHandler{db, fb}, nil
}

// ServeHTTP hanldles all the routes that relates to ".../user" by redirecting to helper functions.
func (u userHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	userBiometricsRegex := regexp.MustCompile(`^/?user/biometrics/?`)

	switch {
	case r.Method == http.MethodPost && userBiometricsRegex.MatchString(r.URL.Path):
		util.ProtectRoute(u.fb, u.db, w, r, u.postUserBiometrics)
	case r.Method == http.MethodGet && userBiometricsRegex.MatchString(r.URL.Path):
		util.ProtectRoute(u.fb, u.db, w, r, u.getUserBiometrics)
	default:
		util.HandleInvalidRoute(w)
	}
}

type userBiometricsPostBody struct {
	WorkoutType        string  `json:"workoutType"`
	CalorieExpectation int     `json:"calorieExpectation"`
	Weight             float32 `json:"weight"`
	Height             float32 `json:"height"`
}

// postUserBiometrics POST ".../user/biometrics"
func (u userHandler) postUserBiometrics(w http.ResponseWriter, r *http.Request, userId int) {
	var biometrics userBiometricsPostBody
	err := json.NewDecoder(r.Body).Decode(&biometrics)
	if util.HandleUserError(err != nil, "invalid request body", w) {
		return
	}

	// Check if FitnessStat has values for the user. If yes, modify, if no, create.
	stmt, err := u.db.Prepare(`
									SELECT FitnessStat.workoutPlanID
									FROM FitnessStat
									WHERE FitnessStat.userID = ?;`,
	)
	if util.HandleServerError(err, "querrying number of fitness stats for user", w) {
		return
	}
	var workoutPlanId zero.Int

	err = stmt.QueryRow(userId).Scan(&workoutPlanId)
	if !workoutPlanId.Valid {
		stmt, err = u.db.Prepare(`INSERT INTO WorkoutPlan(workoutType, workoutCalorieExpect) VALUES(?, ?);`)
		if util.HandleServerError(err, "Insert into WorkoutPlan", w) {
			return
		}
		res, err := stmt.Exec(biometrics.WorkoutType, biometrics.CalorieExpectation)
		if util.HandleServerError(err, "Insert into WorkoutPlan", w) {
			return
		}
		lid, err := res.LastInsertId()

		stmt, err = u.db.Prepare(`INSERT INTO FitnessStat(fitstat_height_inches, fitstat_weight_lbs, workoutPlanID, userID) VALUES(?, ?, ?, ?);`)
		if util.HandleServerError(err, "Insert into FitnessStat", w) {
			return
		}
		_, err = stmt.Exec(biometrics.Height, biometrics.Weight, lid, userId)
		if util.HandleServerError(err, "Insert into FitnessStat", w) {
			return
		}
	} else {
		// Modify this to update field individually.
		if biometrics.Weight != 0 {
			stmt, err = u.db.Prepare(`
										UPDATE FitnessStat
										SET FitnessStat.fitstat_weight_lbs = ?
										WHERE FitnessStat.userID = ?;`)
			if util.HandleServerError(err, "preparing update statement for fitness statistics", w) {
				return
			}
			_, err = stmt.Exec(biometrics.Weight, userId)
			if util.HandleServerError(err, "updating fitness statistics table", w) {
				return
			}

		}
		if biometrics.Height != 0 {
			stmt, err = u.db.Prepare(`
										UPDATE FitnessStat
										SET FitnessStat.fitstat_height_inches= ?
										WHERE FitnessStat.userID = ?;`)
			if util.HandleServerError(err, "preparing update statement for fitness statistics", w) {
				return
			}
			_, err = stmt.Exec(biometrics.Height, userId)
			if util.HandleServerError(err, "updating fitness statistics table", w) {
				return
			}
		}
		if biometrics.WorkoutType != "" {
			stmt, err = u.db.Prepare(`
										UPDATE WorkoutPlan
										SET WorkoutPlan.workoutType = ?
										WHERE WorkoutPlan.workoutPlanID = ?;`)
			if util.HandleServerError(err, "preparing update statment for workout plan", w) {
				return
			}
			_, err = stmt.Exec(biometrics.WorkoutType, workoutPlanId)
			if util.HandleServerError(err, "updating workout plan table", w) {
				return
			}
		}
		if biometrics.CalorieExpectation != 0 {
			stmt, err = u.db.Prepare(`
										UPDATE WorkoutPlan
										SET WorkoutPlan.workoutCalorieExpect = ?
										WHERE WorkoutPlan.workoutPlanID = ?;`)
			if util.HandleServerError(err, "preparing update statment for workout plan", w) {
				return
			}
			_, err = stmt.Exec(biometrics.CalorieExpectation, workoutPlanId)
			if util.HandleServerError(err, "updating workout plan table", w) {
				return
			}
		}
	}
	util.RespondWithSuccess(w, []byte("Successfully insert user biometrics"))
}

type userBiometricsGetBody struct {
	WorkoutType        zero.String `json:"workoutType"`
	CalorieExpectation zero.Int    `json:"calorieExpectation"`
	Weight             zero.Float  `json:"weight"`
	Height             zero.Float  `json:"height"`
	FirstName          zero.String `json:"firstName"`
	LastName           zero.String `json:"lastName"`
	DOB                zero.String `json:"DOB"`
}

// getUserBiometrics get first name, last name, DOB, height, weight, workout type, calorie expect.
func (u userHandler) getUserBiometrics(w http.ResponseWriter, r *http.Request, userId int) {
	var biometrics userBiometricsGetBody
	stmt, err := u.db.Prepare(`
		SELECT User.userFName, User.userLname, User.userDOB, FitnessStat.fitstat_height_inches, FitnessStat.fitstat_weight_lbs, WorkoutPlan.workoutType, WorkoutPlan.workoutCalorieExpect
		FROM User
		LEFT JOIN FitnessStat ON User.userID = FitnessStat.userID
		LEFT JOIN WorkoutPlan ON FitnessStat.workoutPlanID = WorkoutPlan.workoutPlanID
		WHERE User.userID = ?;`,
	)
	if util.HandleServerError(err, "get user biometrics", w) {
		return
	}
	err = stmt.QueryRow(userId).Scan(
		&biometrics.FirstName, &biometrics.LastName, &biometrics.DOB,
		&biometrics.Height, &biometrics.Weight, &biometrics.WorkoutType,
		&biometrics.CalorieExpectation,
	)
	if util.HandleServerError(err, "querrying data", w) {
		return
	}
	biometricsJson, err := json.Marshal(biometrics)
	if util.HandleServerError(err, "marshaling response", w) {
		return
	}
	util.RespondWithSuccess(w, biometricsJson)
}
