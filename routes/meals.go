// Package routes contains all the functions that relate to the routes.
// This specific file handles all the routes that go to ".../meals/..."
package routes

import (
	"database/sql"
	"encoding/json"
	"errors"
	"go_backend/util"
	"net/http"
	"net/url"
	"regexp"
	"time"

	firebase "firebase.google.com/go/v4"
	"github.com/guregu/null/v5/zero"
)

type menuHandler struct {
	db *sql.DB
	fb *firebase.App
}

// NewMenuHandler handles the creation of menuHandler. User should not interact directly with menuHandler.
func NewMenuHandler(fb *firebase.App, db *sql.DB) (http.Handler, error) {
	if db == nil {
		return nil, errors.New("provided database is nil")
	}
	if fb == nil {
		return nil, errors.New("provided firebase app is nil")
	}
	return menuHandler{db, fb}, nil
}

// ServeHTTP handles serving all the requests that relate to ".../meals" by redirecting to helper functions.
func (m menuHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	mealsFoodRegex := regexp.MustCompile(`^/?meals/food/?`)
	mealsHistoryRegex := regexp.MustCompile(`^/?meals/history/?`)
	mealsPlanRegex := regexp.MustCompile(`^/?meals/plan/?`)

	// This route does not require authentication.
	if request.Method == http.MethodGet && mealsFoodRegex.MatchString(request.URL.Path) {
		m.getFood(writer, request)
		return
	}

	switch {
	case request.Method == http.MethodPost && mealsHistoryRegex.MatchString(request.URL.Path):
		util.ProtectRoute(m.fb, m.db, writer, request, m.postMealsHistory)
	case request.Method == http.MethodDelete && mealsHistoryRegex.MatchString(request.URL.Path):
		util.ProtectRoute(m.fb, m.db, writer, request, m.deleteMealsHistory)
	case request.Method == http.MethodGet && mealsPlanRegex.MatchString(request.URL.Path):
		util.ProtectRoute(m.fb, m.db, writer, request, m.getMealsPlan)
	case request.Method == http.MethodGet && mealsHistoryRegex.MatchString(request.URL.Path):
		util.ProtectRoute(m.fb, m.db, writer, request, m.getMealHistory)

	default:
		util.HandleInvalidRoute(writer)
	}
}

type foodItem struct {
	FoodId          uint        `json:"foodId"`
	FoodName        zero.String `json:"foodName"`
	FoodCalorie     zero.Float  `json:"foodCalorie"`
	FoodFat         zero.Float  `json:"foodFat"`
	FoodCholesterol zero.Float  `json:"foodCholesterol"`
	FoodSodium      zero.Float  `json:"foodSodium"`
	FoodCarb        zero.Float  `json:"foodCarb"`
	FoodSugar       zero.Float  `json:"foodSugar"`
	FoodProtein     zero.Float  `json:"foodProtein"`
	FoodServingTime zero.String `json:"foodServingTime"`
	FoodLocation    zero.String `json:"foodLocation"`
}

// getFood GET ".../meals/food/?date={YYYYmmdd}"
func (m menuHandler) getFood(w http.ResponseWriter, r *http.Request) {
	date, meaningful := util.GetRouteDateParameter(r.URL.RawQuery, w)
	if !meaningful {
		return
	}

	stmt :=
		`SELECT Food.*
		 FROM Food
		 INNER JOIN FoodDate ON Food.foodId = FoodDate.foodId
		 WHERE FoodDate.foodDateDate = CAST(? as DATE);`

	rows, err := m.db.Query(stmt, date.Format(time.DateOnly))
	if util.HandleServerError(err, "Getting data", w) {
		return
	}
	defer rows.Close()

	foodsMap := make(map[string][]foodItem)
	foodsMap["food_items"] = make([]foodItem, 0)
	for rows.Next() {
		var newFood foodItem
		err := rows.Scan(&newFood.FoodId, &newFood.FoodName, &newFood.FoodCalorie, &newFood.FoodFat,
			&newFood.FoodCholesterol, &newFood.FoodSodium, &newFood.FoodCarb,
			&newFood.FoodSugar, &newFood.FoodProtein, &newFood.FoodServingTime, &newFood.FoodLocation)

		if util.HandleServerError(err, "Reading from database", w) {
			return
		}
		foodsMap["food_items"] = append(foodsMap["food_items"], newFood)
	}

	foodsJson, err := json.Marshal(foodsMap)
	if util.HandleServerError(err, "Marsheling database data", w) {
		return
	}
	util.RespondWithSuccess(w, foodsJson)
}

type mealItem struct {
	MealId   uint     `json:"mealId"`
	FoodItem foodItem `json:"foodItem"`
}

// getMealHistory GET ".../meals/history/?date={YYYYmmdd}"
func (m menuHandler) getMealHistory(w http.ResponseWriter, r *http.Request, userId int) {
	date, meaningful := util.GetRouteDateParameter(r.URL.RawQuery, w)
	if !meaningful {
		return
	}
	stmt := `
		SELECT Meal.mealID, Food.*
		FROM Meal
		INNER JOIN FoodDate A ON A.foodDateID = Meal.foodDateID
		LEFT JOIN Food ON A.foodID = Food.foodID
		WHERE A.foodDateDate = CAST(? AS DATE)
		AND Meal.userID = ?;`

	rows, err := m.db.Query(stmt, date.Format(time.DateOnly), userId)
	if util.HandleServerError(err, "Getting data", w) {
		return
	}
	defer rows.Close()

	mealsMap := make(map[string][]mealItem)
	mealsMap["meal_items"] = make([]mealItem, 0)
	for rows.Next() {
		var newMeal mealItem
		err := rows.Scan(&newMeal.MealId, &newMeal.FoodItem.FoodId, &newMeal.FoodItem.FoodName, &newMeal.FoodItem.FoodCalorie, &newMeal.FoodItem.FoodFat,
			&newMeal.FoodItem.FoodCholesterol, &newMeal.FoodItem.FoodSodium, &newMeal.FoodItem.FoodCarb,
			&newMeal.FoodItem.FoodSugar, &newMeal.FoodItem.FoodProtein, &newMeal.FoodItem.FoodServingTime, &newMeal.FoodItem.FoodLocation)

		if util.HandleServerError(err, "Reading from database", w) {
			return
		}
		mealsMap["meal_item"] = append(mealsMap["meal_item"], newMeal)
	}

	foodsJson, err := json.Marshal(mealsMap)
	if util.HandleServerError(err, "Marsheling database data", w) {
		return
	}
	util.RespondWithSuccess(w, foodsJson)
}

type mealsHistoryBody struct {
	FoodID   uint   `json:"foodID"`
	FoodDate string `json:"foodDate"`
}

// postMealsHistory POST ".../meals/history/"
func (m menuHandler) postMealsHistory(w http.ResponseWriter, r *http.Request, userId int) {
	// Step one: Find the foodDateID that corresponds to the food id and todays date.
	var body mealsHistoryBody
	err := json.NewDecoder(r.Body).Decode(&body)
	if util.HandleUserError(err != nil, "Invalid Request Body", w) {
		return
	}
	currDate, err := time.Parse(time.DateOnly, body.FoodDate)
	if util.HandleUserError(err != nil, "Invalid date format", w) {
		return
	}
	stmt := `
			SELECT FoodDate.foodDateID
			FROM FoodDate
			WHERE FoodDate.foodDateDate = CAST(? as DATE)
			AND FoodDate.foodID = ?;`

	row := m.db.QueryRow(stmt, currDate, body.FoodID)
	var foodDateID int
	err = row.Scan(&foodDateID)
	if util.HandleServerError(err, "Querying database", w) {
		return
	}
	// Step two: Add teh foodId, foodDateId, userID into the meals table.
	execStmt, err := m.db.Prepare(`INSERT INTO Meal(foodID, userID, foodDateID) VALUES (?, ?, ?)`)
	if util.HandleServerError(err, "adding new meal", w) {
		return
	}
	defer execStmt.Close()

	_, err = execStmt.Exec(body.FoodID, userId, foodDateID)

	if util.HandleServerError(err, "Inserting a new food into the meal table", w) {
		return
	}
	util.RespondWithSuccess(w, []byte("Sucessfully add meal to user's meal history."))
}

// deleteMealsHistory DELETE ".../meals/history/?foodId={mealId}"
func (m menuHandler) deleteMealsHistory(w http.ResponseWriter, r *http.Request, userId int) {
	query, err := url.ParseQuery(r.URL.RawQuery)
	if util.HandleServerError(err, "Parsing request URL", w) {
		return
	}
	mealId := query.Get("foodId")
	if util.HandleUserError(mealId == "", "missing Id parameter", w) {
		return
	}
	stmt, err := m.db.Prepare(`DELETE FROM Meal WHERE Meal.mealID = ? AND Meal.userID = ?`)
	if util.HandleServerError(err, "delete from meal plan", w) {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(mealId, userId)
	if util.HandleServerError(err, "removing meal from the database", w) {
		return
	}
	util.RespondWithSuccess(w, []byte("Successfully remove meal"))
}

// getMealsPlan GET ".../meals/plan/"
func (m menuHandler) getMealsPlan(w http.ResponseWriter, r *http.Request, userId int) {
	panic("implement me")
}
