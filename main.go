package main

import (
	"context"
	"database/sql"
	"firebase.google.com/go/v4"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"go_backend/routes"
	"log"
	"net/http"
	"os"
)

// Before running, the following environment variables must be set:
// DATABASE_URI, containing the URI throgh which to access the database.
// FIREBASE_CONFIG containing the path to the json file containing the firebase credentials.
func main() {
	// Start database connnection.
	fmt.Printf("Starting server.\n")

	sqlUri := fmt.Sprintf("root:%s@tcp(vtmealplandb-mysql-headless:3306)/vtmealplandb", os.Getenv("MYSQL_ROOT_PASSWORD"))
	db, err := sql.Open("mysql", sqlUri)
	defer db.Close()

	if err != nil {
		log.Fatalf("Failed to open database connection due to %s", err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatalf("Failed to initialize database connection due to %s", err)
	}

	// Setup firebase.
	app, err := firebase.NewApp(context.Background(), nil)
	if err != nil {
		log.Fatalf("Failed to start firebase app due to:\n %s\n", err)
	}

	// Declare routes.
	authHandler, err := routes.NewAuthHandler(app, db)
	if err != nil {
		log.Fatalf("Failed to create route due to %s", err)
	}

	menuHandler, err := routes.NewMenuHandler(app, db)
	if err != nil {
		log.Fatalf("Failed to create route due to %s", err)
	}

	userHandler, err := routes.NewUserHandler(app, db)
	if err != nil {
		log.Fatalf("Failed to create route due to %s", err)
	}

	mux := http.NewServeMux()
	mux.Handle("/auth/", authHandler)
	mux.Handle("/meals/", menuHandler)
	mux.Handle("/user/", userHandler)

	// Initialize server.
	err = http.ListenAndServe("0.0.0.0:8080", mux)
	if err != nil {
		log.Fatalf("Failed to start server due to:\n %s\n", err)
	}
}
